<?php

function eventById($bdd, $id) {
    $request = $bdd->prepare('SELECT * FROM events WHERE id=:id');
    $request->execute(array(
        ':id' => $id
    ));

    return $request->fetchAll()[0];
}

function eventsDate($bdd, $date) {
    $request = $bdd->prepare('SELECT * FROM events WHERE DATE(startdate)=:date');
    $request->execute(array(
        'date' => $date
    ));

    return $request->fetchAll();
}

function nbEventsDate($bdd, $date) {
    $request = $bdd->prepare('SELECT COUNT(*) FROM events WHERE DATE(startdate)=:date');
    $request->execute(array(
        'date' => $date
    ));

    return $request->fetchAll()[0][0];
}

function eventsOrganizer($bdd, $date, $organizer_id) {
    $request = $bdd->prepare('SELECT * FROM events WHERE DATE(startdate)=:date AND organizer_id=:organizer_id');
    $request->execute(array(
        'date' => $date,
        'organizer_id' => $organizer_id
    ));

    return $request->fetchAll();
}

function nbEventsOrganizer($bdd, $date, $organizer_id) {
    $request = $bdd->prepare('SELECT COUNT(*) FROM events WHERE DATE(startdate)=:date AND organizer_id=:organizer_id');
    $request->execute(array(
        'date' => $date,
        'organizer_id' => $organizer_id
    ));

    return $request->fetchAll()[0][0];
}

function addEvent($bdd, $name, $description, $startdate, $enddate, $organizer_id, $nb_place) {
    $request = $bdd->prepare('INSERT INTO events (name, description, startdate, enddate, organizer_id, nb_place) VALUE (:name, :description, :startdate, :enddate, :organizer_id, :nb_place)');
    $response = $request->execute(array(
        ':name' => $name,
        ':description' => $description,
        ':startdate' => $startdate,
        ':enddate' => $enddate,
        ':organizer_id' => $organizer_id,
        ':nb_place' => $nb_place
    ));

    return $response;
}

function removeEvent($bdd, $id) {
    $requestUsers = $bdd->prepare('DELETE FROM user_participates_events WHERE id_event=:id_event');
    $requestEvent = $bdd->prepare('DELETE FROM events WHERE id=:id');

    if($requestUsers->execute(array(
        ':id_event' => $id
    ))) {
        return $requestEvent->execute(array(
            ':id' => $id
        ));
    }

    return false;
}

function userParticipates($bdd, $event, $id) {
    $request = $bdd->prepare('SELECT * FROM user_participates_events WHERE id_event=:id_event AND id_participant=:id');
    $request->execute(array(
        ':id_event' => $event['id'],
        ':id' => $id
    ));
}

function isFull($bdd, $event) {
    $requestNb = $bdd->prepare('SELECT COUNT(*) FROM user_participates_events WHERE id_event=:id_event');
    $requestNb->execute(array(
        ':id_event' => $event['id']
    ));

    return $requestNb->fetchAll()[0][0] == $event['nb_place'];
}