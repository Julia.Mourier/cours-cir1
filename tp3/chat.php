<?php
$name = $message = "";

try {
    $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
    $baseDonnee = new PDO('mysql:host=localhost;dbname=chat;charset=utf8', 'root', 'isencir', $opts);

    if ((isset($_POST['name']))&&(isset($_POST['message']))) {
        if ((empty($_POST['name']))||(empty($_POST['message']))) {//s'il n'y a rien on affiche un message d'erreur
            echo "Veuillez entrer votre message et pseudo dans le format voulu";
        } else {
            $name = $_POST['name'];
            $message = $_POST['message'];
            $date = date("Y-m-d H:i:s");

            $request = $baseDonnee->prepare("INSERT INTO messages (message, name, date) VALUES (:message, :name, :date)");
            $request->execute(array(
              ':message' => $message,
              ':name' => $name,
              ':date' => $date
            ));
        }
    }

} catch(PDOException $e) {
    die('Probleme de connexion à la bdd: ' . $e->getMessage());
}

?>
