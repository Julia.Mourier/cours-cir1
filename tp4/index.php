<?php

session_start();

include('./models/user.php');

try {
  $opts = [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];
  $bdd = new PDO('mysql:host=localhost;dbname=event_calendar;charset=utf8', 'root', 'isencir', $opts);
} catch (PDOException $e) {
  die('Erreur lors de la connection à la base de donnée : ' . $e->getMessage());
}

if(!isUserConnected()) {
  include('./controllers/connection.php');
} else {
  if(isset($_GET['logout'])) {
    logout();
  }

  $user = getUser($bdd, $_SESSION['userId']);

  if($user['rank'] == 'ORGANIZER') {
    include('./controllers/organizer.php');
  }

  if($user['rank'] == 'CUSTOMER') {
    include('./controllers/customer.php');
  }
}
