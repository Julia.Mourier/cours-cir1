<?php

function FirstDayOfMonth($month,$year){
  $day = (date('D', mktime(0, 0, 0, $month, 1, $year)));
  return array_search($day, ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]) + 1;
}

function LastDayOfMonth($month, $year){
  $day = date('t', strtotime("$year-$month"));
  return $day + 1;
}