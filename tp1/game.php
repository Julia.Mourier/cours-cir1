<?php

$taille=22;

function initTab($array,$taille){
    for ($i = 1;$i < $taille - 1;$i++) { //on veut que la grille interieure
        $array[$i] = array();
        for ($j = 1;$j < $taille - 1;$j++) {
            if (rand(0, 1)){
                $array[$i][$j] = 1;
            } else {
                $array[$i][$j] = 0;
            }
        }
    }
    
    for($i=0;$i<=$taille;$i++){ //on supprime les valeurs des cases exterieurs
        for($j=0;$j<=$taille;$j++){
            if ($i===0||$i===$taille||$j===0||$j===$taille) {
                $array[$i][$j]=0;
            }
        }
    }
    return $array;
}

function initTabaO($array,$taille){
    for ($i = 0;$i < $taille;$i++) { 
        $array[$i] = array();
        for ($j = 0;$j < $taille;$j++) {
                $array[$i][$j] = 0;
        }
    }
    return $array;
}

function calculVoisins($array, $i, $j){
    $test = $array[$i-1][$j-1] + $array[$i-1][$j] + $array[$i-1][$j+1] + $array[$i][$j-1] + $array[$i][$j+1] + $array[$i+1][$j-1] + $array[$i+1][$j] + $array[$i+1][$j+1];
    return $test;
}

function mort($taille,$array){ //passage de vie a mort
    $attenteMort = initTabaO(array(), $taille);
    for($i = 1 ;$i <= $taille - 1;$i++){
        for($j = 1;$j <= $taille - 1;$j++){
            $test= calculVoisins($array,$i,$j);
            if($test != 2 && $test != 3 && $array[$i][$j]===1){
                $attenteMort[$i][$j] = 1;
            }
        }
    }
    return $attenteMort;
}

function vie($taille,$array){ //passage de mort a vie
    $attenteVie = initTabaO(array(), $taille);
    for($i=1;$i<=$taille-1;$i++){
        for($j=1;$j<=$taille-1;$j++){
            $test= calculVoisins($array, $i, $j);
            if($test===3 && $array[$i][$j]===0){
                $attenteVie[$i][$j] = 1;

            } 
        }
    }
    return $attenteVie;
}

function nvGenerat($attenteVie, $attenteMort, $array, $taille){//tableau representatif de la nouvelle generation de cellules
    $nvGeneration = initTabaO(array(), $taille);
    for($i=1;$i<=$taille-1;$i++){
        for($j=1;$j<=$taille-1;$j++){
             $nvGeneration[$i][$j] = ($array[$i][$j] + $attenteMort[$i][$j] + $attenteVie[$i][$j]) % 2; //on ne s'occupe pas de ce qui survive et reste mort car il ne change pas d'état
        }
    }
    return $nvGeneration;
}