<?php

function existsUser($bdd, $username) {
  $request = $bdd->prepare('SELECT * FROM Users WHERE login=:login');
  $request->execute(array(
    ':login' => $username
  ));

  return ($request->rowCount() > 0);
}

function createUser($bdd, $username, $password, $rank) {
  if(existsUser($bdd, $username)) {return false;}

  $request = $bdd->prepare('INSERT INTO Users (login, password, rank) VALUES (:login, :password, :rank)');
  return $request->execute(array(
    ':login' => $username,
    ':password' => password_hash($password, PASSWORD_DEFAULT),
    ':rank' => $rank
  ));
}

function verifyUser($bdd, $username, $password) {
  if(!existsUser($bdd, $username)) {return false;}

  $requestPassword = $bdd->prepare('SELECT * FROM Users WHERE login=:login');
  $requestPassword->execute(array(
    ':login' => $username
  ));

  $userData = $requestPassword->fetch();

  if(password_verify($password, $userData['password'])) {
    $_SESSION['userId'] = $userData['id'];
    return true;
  } else {
    return false;
  }
}

function getUser($bdd, $id) {
  $request = $bdd->prepare('SELECT * FROM Users WHERE id=:id');
  $request->execute(array(
    ':id' => $id
  ));

  return $request->fetch();
}

function isUserConnected() {
  return isset($_SESSION['userId']);
}

function logout() {
  $_SESSION = array();
  die(header('location: ./'));
}