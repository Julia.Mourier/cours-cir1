<?php

if(isset($_GET['form'])) {
  if($_GET['form'] == 'signup') {
    $username = filter_input(INPUT_POST, 'username');
    $password = filter_input(INPUT_POST, 'password');
    $password2 = filter_input(INPUT_POST, 'password2');
    $rank = filter_input(INPUT_POST, 'rank');

    if($password != $password2) {
      // Erreur: passwords not match
      echo("Please put the same password in the fields");
      die(include('./vues/signup.html'));
    }

    if(!createUser($bdd, $username, $password, $rank)) {
      // Erreur: can't create the account
      echo("Unable to register");
      die(include('./vues/signup.html'));
    }

    echo("Successly signed up");
    die(header('location: ./'));
  } else if($_GET['form'] == 'login') {
    $username = filter_input(INPUT_POST, 'username');
    $password = filter_input(INPUT_POST, 'password');

    if(!verifyUser($bdd, $username, $password)) {
      echo("Bad credidentials");
      die(include('./vues/login.html'));
    }

    echo("Successly logged");
    die(header('location: ./'));
  }
} else {
  if(isset($_GET['login'])) {
    include('./vues/login.html');
  } else if(isset($_GET['signup'])) {
    include('./vues/signup.html');
  } else {
    include('./vues/home.html');
  }
}
