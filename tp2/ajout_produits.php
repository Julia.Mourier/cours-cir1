
<?php
$numbprod = $name = $price = "";


define('TARGET_DIRECTORY', './uploads/');

if (!empty($_FILES['photo'])) {
	move_uploaded_file($_FILES['photo']['tmp_name'], TARGET_DIRECTORY . $_FILES['photo']['name']);
}

if (isset($_POST['name']) && isset($_POST['price']) && isset($_POST['numbprod'])) {
	$name = htmlspecialchars($_POST['name'], 3);
	$price = htmlspecialchars($_POST['price'], 3);
	$numbprod = htmlspecialchars($_POST['numbprod'], 3);

	// verification des caracteres
	if ((!preg_match("/^[a-zA-Z0-9 ]{2,30}$/", $name)) || (!preg_match("/^[^-]\s*[0-9]*.?[0-9]{0,2}+$/", $price)) || (!preg_match("/^[0-9]{0,9}$/", $numbprod))) {
		die("Les données envoyées ne sont pas au format voulu");
	}

	$tableau_produit = array(
		$name,
		$price,
		$numbprod,
		TARGET_DIRECTORY . $_FILES['photo']['name']
	);
	$handle = fopen('produits.csv', 'a');
	fputcsv($handle, $tableau_produit, ';');
	fclose($handle);

	header('location: ./');
}

?>
