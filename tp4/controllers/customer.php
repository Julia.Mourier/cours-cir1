<?php 

include('./functions.php');
include('./models/event.php');

if(!isset($_SESSION['today'])) {
  $_SESSION['today'] = strtotime('today');
}

$today = $_SESSION['today'];

$year = date("Y", $today);
$month = date("m", $today);
$day = date("D", $today);

if(isset($_GET['previous'])) {
  $_SESSION['today'] = strtotime('-1 month', $today);
  header('location: ./');
}

if(isset($_GET['next'])) {
  $_SESSION['today'] = strtotime('+1 month', $today);
  header('location: ./');
}

$first = FirstDayOfMonth($month, $year);
$last = LastDayOfMonth($month, $year);

$days = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
$months = array("January","February","March","April","May","June","July","August","September","October","November","December");

if(isset($_GET['showEvent'])) {
    $id = filter_input(INPUT_GET, 'showEvent');
    $event = eventById($bdd, $id);

    include('./vues/eventCustomer.html');
} else if(isset($_GET['showMore'])) {

} else {
  include('./vues/calendarCustomer.html');
}