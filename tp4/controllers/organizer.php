<?php 

include('./functions.php');
include('./models/event.php');

if(!isset($_SESSION['today'])) {
  $_SESSION['today'] = strtotime('today');
}

$today = $_SESSION['today'];

$year = date("Y", $today);
$month = date("m", $today);
$day = date("D", $today);

if(isset($_GET['previous'])) {
  $_SESSION['today'] = strtotime('-1 month', $today);
  header('location: ./');
}

if(isset($_GET['next'])) {
  $_SESSION['today'] = strtotime('+1 month', $today);
  header('location: ./');
}

$first = FirstDayOfMonth($month, $year);
$last = LastDayOfMonth($month, $year);

$days = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
$months = array("January","February","March","April","May","June","July","August","September","October","November","December");

if(isset($_GET['createEvent'])) {
  $day = filter_input(INPUT_GET, 'createEvent');
  
  include('./vues/createEvent.html');
} else if(isset($_GET['showEvent'])) {
  $id = filter_input(INPUT_GET, 'showEvent');
  $event = eventById($bdd, $id);

  include('./vues/eventOrganizer.html');
} else if(isset($_GET['showMore'])) {
  $date = filter_input(INPUT_GET, 'showMore');
  $events = eventsOrganizer($bdd, $date, $user['id']);

  include('./vues/showMoreOrganizer.html');
} else if(isset($_GET['removeEvent'])) {
  $id = filter_input(INPUT_GET, 'removeEvent');

  removeEvent($bdd, $id);

  header('location: ./');
} else if(isset($_GET['registerEvent'])) {
  $name = filter_input(INPUT_POST, 'eventname');
  $description = filter_input(INPUT_POST, 'description');
  $startdate = str_replace('T', ' ', filter_input(INPUT_POST, 'startdate')) . ':00';
  $enddate = str_replace('T', ' ', filter_input(INPUT_POST, 'enddate')) . ':00';
  $organizer_id = $user['id'];
  $nb_place = filter_input(INPUT_POST, 'placeNumber');
  
  addEvent($bdd, $name, $description, $startdate, $enddate, $organizer_id, $nb_place);

  header('location: ./');
} else {
  include('./vues/calendar.html');
}