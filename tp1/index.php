<?php
    include 'game.php';
    
    session_start();
    
    //initialisation de la grille à 0
    if(!isset($_SESSION['array'])) {
        $array = initTab(array(), $taille);
        $_SESSION['array'] = $array;
    } else {
        $array = $_SESSION['array'];
    }
    
    //premiere génération de cellule générées aléatoirement
    //création de la nouvelle génération
    
    $attenteVie = vie($taille, $array);
    $attenteMort = mort($taille, $array);
    
   //la nouvelle génération est ainsi créée 
    $array = nvGenerat($attenteVie, $attenteMort, $array, $taille);
   
    $_SESSION['array'] = $array;
    
    //affichage de la nouvelle generation
    include 'grid_template.html';
    
    
    
    
    
    
?>